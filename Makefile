################################################
#         Generic Makefile Template
#
#  When using, need to set up some variables,
#   the condition and the file list
#
#  Author: hone
################################################

CC      = gcc
CFLAGS += -g
CFLAGS += -Wall
CFLAGS += -O0
CFLAGS += -D_REENTRANT

#CFLAGS += -Wno-unused-variable
#CFLAGS += -Wno-unused-but-set-variable

## 目标可执行文件 ## 相关目标文件列表
TARGET = httpd
MIDDLE = httpd.o

## 目标动态链接库 ## 相关目标文件列表
LIB_SO =
LIBMDO =
## 目标动态链接库 编译时使用的相关参数
OUT_SHARED_LIBS += -fPIC -shared -Wl,-rpath,./ -L./ -lpthread

## 静态和动态编译时相关参数
SYS_SHARED_LIBS += -lpthread
STATIC_COM_ARGS +=
SHARED_COM_ARGS += -Wl,-rpath,./ -L./

.PHONY: all clean clear cmake remake

#strip $(LIB_SO)

## 信息显示控制
HINT = @
CONT = @
CCOM = $(HINT) printf '    \e[34mCC\e[m \e[33m%s\e[m\n' $@
LINK = $(HINT) printf '    \e[31mLINK\e[m \e[32m%s\e[m\n' $@

## 编译参数 ## 显示详细的编译细节
## make v=y
ifeq "${v}" "y"
	CCOM =
	LINK =
	CONT =
endif

## 编译命令 ## 静态和动态编译命令
COMMON_STATIC = $(CONT) $(CC) $(CFLAGS) $^ $(SYS_SHARED_LIBS) $(STATIC_COM_ARGS) -o $@
COMMON_SHARED = $(CONT) $(CC) $(CFLAGS) $^ $(SYS_SHARED_LIBS) $(SHARED_COM_ARGS) -o $@

## 目标文件编译 ##
.c.o:
	$(CCOM)
	$(CONT) $(CC) -c $(CFLAGS) $< -o $@

all: $(TARGET)
	make -f Makefile.ep

so: $(LIB_SO)
$(LIB_SO): $(LIBMDO)
	$(LINK)
	$(CONT) $(CC) $(CFLAGS) $(LIBMDO) $(OUT_SHARED_LIBS) -o $(LIB_SO)

## 条件编译 ## 静态或动态编译选项
## make s=y ## 编译并使用动态库
ifeq "${s}" "y"
CFLAGS += -fPIC
$(TARGET): $(MIDDLE) $(LIB_SO)
	$(LINK)
	$(COMMON_SHARED)
else
$(TARGET): $(MIDDLE) $(LIBMDO)
	$(LINK)
	$(COMMON_STATIC)
endif

## 如果终极命令列表存是 libso, 加上参数 -fPIC
ifeq ($(MAKECMDGOALS),libso)
CFLAGS += -fPIC
endif
libso: $(LIB_SO)
	@mkdir -p lib include && cp $(LIB_SO) lib && cp yhsocket.h yhevent.h include
	@echo "Create: include lib"

lib:
	make cleanlib
	make libso

clear:
	clear

cmake:
	make clear
	make all

remake:
	make clear
	make clean
	make all

clean:
	rm -rf $(TARGET) $(MIDDLE) $(LIB_SO) $(LIBMDO) include lib -rf
	make -f Makefile.ep clean
cleanlib:
	rm -rf $(LIB_SO) $(LIBMDO) include lib -rf
cleanall:
	make clean
	make cleanlib

